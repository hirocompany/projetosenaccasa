using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class clickEventListener : MonoBehaviour {

	private Vector3 pos;
	private Vector3 posCamera;
	private Vector3 screenPos;
	private bool touchEnter = false;
	private RaycastHit2D hit2d;
	private clickEvent ce = null;
	public bool drag = true;
	public Text textDebug;

	void Start() 
	{
		StartCoroutine(clickEvent());
	}

	public IEnumerator clickEvent()
	{
		while(true)
		{
			if(Input.touchCount > 0)
			{
				pos = Input.GetTouch(0).position;
				sendClick();
			}
			else if(touchEnter)
			{
				touchEnter = false;
				if(ce != null)
				{
					ce.unclick();
					ce = null;
				}
			}
			else if(Input.GetMouseButton(0))
			{
				pos = Input.mousePosition;
				sendClick();
			}
			if (Input.GetKeyDown(KeyCode.Escape)) 
			{
				if(Application.loadedLevel == 0)
				{
					Application.Quit(); 
				}
				else
				{
					Application.LoadLevel(0);
				}
			}
			yield return new WaitForEndOfFrame();
		}
	}
	void sendClick()
	{
		screenPos = Camera.main.ScreenToWorldPoint(pos);
		posCamera = new Vector3(screenPos.x, screenPos.y, 50.0f);
		hit2d = Physics2D.Raycast(screenPos, Vector2.zero);
		if (hit2d.collider.GetComponent<clickEvent>() != null)
		{
				if(!touchEnter)
				{
					ce = (clickEvent)hit2d.collider.GetComponent(typeof(clickEvent));
					Debug.Log(drag);
					drag = ce.drag;
					ce.click();
					touchEnter = true;
				}
				else if(drag)
				{
					ce.click();
				}

		}
	}

}
