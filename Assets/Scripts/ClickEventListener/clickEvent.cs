using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class clickEvent : MonoBehaviour {

	public GameObject[] target;
	public string[] definir;
	public GameObject[] untarget;
	public string[] undefinir;
	public bool drag;
    public string functionName = "click";
    public string functionUnclickName = "unclick";

	
	private Vector3 pos;
	private Vector3 posCamera;
	private Vector3 screenPos;
	private bool touchEnter = false;
	private BoxCollider2D box;
	private RectTransform _thisBox;

	public bool oldTime;

	void Start()
	{
		StartCoroutine(setSize());
	}
	void Update()
	{
		if(oldTime)
		{
			if(Input.touchCount > 0)
			{
				pos = Input.GetTouch(0).position;
				screenPos = Camera.main.ScreenToWorldPoint(pos);
				posCamera = new Vector3(screenPos.x, screenPos.y, 50.0f);
				if(drag)
				{
					if (box == Physics2D.OverlapPoint(posCamera))
					{
						touchEnter = true;
						for(int i = 0; i < target.Length; ++i)
						{
							if(definir[i] == "")
								target[i].SendMessage(functionName);
							else
								target[i].SendMessage(functionName, definir[i]);
						}
					}
				}
				else
				{
					
					if (box == Physics2D.OverlapPoint(posCamera) && touchEnter == false)
					{
						touchEnter = true;
						for(int i = 0; i < target.Length; ++i)
						{
							if(definir[i] == "")
								target[i].SendMessage(functionName);
							else
								target[i].SendMessage(functionName, definir[i]);
						}
					}
				}
			}
			else if(touchEnter)
			{
				touchEnter = false;
				for(int i = 0; i < untarget.Length; ++i)
				{
					if(undefinir[i] == "")
						untarget[i].SendMessage(functionUnclickName);
					else
						untarget[i].SendMessage(functionUnclickName, undefinir[i]);
				}
			}
			else if(Input.GetMouseButton(0))
			{
				pos = Input.mousePosition;
				screenPos = Camera.main.ScreenToWorldPoint(pos);
				posCamera = new Vector3(screenPos.x, screenPos.y, 50.0f);
				if(drag)
				{
					if (box == Physics2D.OverlapPoint(posCamera))
					{
						touchEnter = true;
						for(int i = 0; i < target.Length; ++i)
						{
							if(definir[i] == "")
								target[i].SendMessage(functionName);
							else
								target[i].SendMessage(functionName, definir[i]);
						}
					}
				}
				else
				{
					
					if (box == Physics2D.OverlapPoint(posCamera) && touchEnter == false)
					{
						touchEnter = true;
						for(int i = 0; i < target.Length; ++i)
						{
							if(definir[i] == "")
								target[i].SendMessage(functionName);
							else
								target[i].SendMessage(functionName, definir[i]);
						}
					}
				}
			}
		}
	}

	IEnumerator setSize()
	{
		yield return new WaitForEndOfFrame();
		box = this.GetComponent<BoxCollider2D>();
		yield return new WaitForEndOfFrame();
		try
		{
			_thisBox = this.GetComponent<RectTransform>();
			box.size = new Vector2(_thisBox.rect.width, _thisBox.rect.height);
		}
		catch
		{

		}
		yield break;
	}

	public void click()
	{
		if(drag)
		{
			touchEnter = true;
			for(int i = 0; i < target.Length; ++i)
			{
				if(definir[i] == "")
					target[i].SendMessage(functionName);
				else
					target[i].SendMessage(functionName, definir[i]);
			}
		}
		else
		{
			if(touchEnter == false)
			{
				touchEnter = true;
				for(int i = 0; i < target.Length; ++i)
				{
					if(definir[i] == "")
						target[i].SendMessage(functionName);
					else
						target[i].SendMessage(functionName, definir[i]);
				}
			}
		}
	}
	public void unclick()
	{
		touchEnter = false;
		for(int i = 0; i < untarget.Length; ++i)
		{
			if(undefinir[i] == "")
				untarget[i].SendMessage(functionUnclickName);
			else
				untarget[i].SendMessage(functionUnclickName, undefinir[i]);
		}
	}
}
