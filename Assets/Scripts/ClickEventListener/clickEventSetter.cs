using UnityEngine;
using System.Collections;

public class clickEventSetter : MonoBehaviour {

	public string[] objectName;
	public string[] clickMessenge;
	public clickEvent ce;
	public string[] objectUnclick;
	public string[] unclickMessenge;
    public string functionName = "click";
    public string functionUnclickName = "unclick";

	// Use this for initialization
	void Start () {
		ce.target = new GameObject[objectName.Length];
		for(int i = 0 ; i < objectName.Length; ++i)
		{
			ce.target[i] = GameObject.Find(objectName[i]);
			ce.definir[i] = clickMessenge[i];
		}
		if(objectUnclick.Length > 0)
		{
			ce.untarget = new GameObject[objectUnclick.Length];
			for(int i = 0 ; i < objectName.Length; ++i)
			{
				ce.untarget[i] = GameObject.Find(objectUnclick[i]);
				ce.undefinir[i] = unclickMessenge[i];
			}
		}
	}
}
